(function () {
    // Propiedades Tabs
    var propTabs = {
        primerHeader: document.getElementById('header-menu').firstElementChild,
        primerContenido: document.getElementById('content-menu').firstElementChild,
        enlacesHeader: document.querySelectorAll('#header-menu li a'),
        liHeader: document.querySelectorAll('#header-menu li'),
        divsContenido: document.querySelectorAll('#content-menu > div'),
        contenidoActivo: null
    }
    // Metodos Tabs
    var methodTabs = {
        incio: function () {
            propTabs.primerHeader.className = 'active';
            propTabs.primerContenido.className = 'active';

            for (var i = 0; i < propTabs.enlacesHeader.length; i++) {
                propTabs.enlacesHeader[i].addEventListener('click', function (e) {

                    for (var j = 0; j < propTabs.liHeader.length; j++) {
                        propTabs.liHeader[j].className = '';
                    }

                    for (var j = 0; j < propTabs.divsContenido.length; j++) {
                        propTabs.divsContenido[j].className = '';
                    }

                    this.parentElement.className = 'active';
                    propTabs.contenidoActivo = this.getAttribute('href');
                    document.querySelector(propTabs.contenidoActivo).className = 'active';
                    document.querySelector(propTabs.contenidoActivo).style.opacity = 0;

                    setTimeout(function () {
                        document.querySelector(propTabs.contenidoActivo).style.opacity = 1;
                    }, 100);

                    e.preventDefault();

                });
            }
        }
    }

    methodTabs.incio();
})();