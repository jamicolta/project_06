(function () {
    // Objeto con las Propiedades del efecto Lightbox.
    const propLightbox = {
        imgContainer: document.getElementsByClassName('lightbox'),
        imagen: null,
        imagenSrc: null,
        cuerpoDom: document.getElementsByTagName('body')[0],
        lightbox_container: null,
        modal: null,
        cerrarModal: null,
        animacion: 'fade'
    }

    // Objeto con los Metodos del efecto Lightbox.
    const methodLightbox = {
        inicio: function () {
            for (let i = 0; i < propLightbox.imgContainer.length; i++) {
                propLightbox.imgContainer[i].addEventListener('click', methodLightbox.capturarImagen);
            }
        },
        capturarImagen: function () {
            propLightbox.imagen = this;
            methodLightbox.lightbox(propLightbox.imagen);
        },
        lightbox: function (imagen) {
            propLightbox.imagenSrc = window.getComputedStyle(imagen, null).backgroundImage.slice(5, -2);
            propLightbox.cuerpoDom.appendChild(document.createElement('div')).setAttribute('id', 'lightbox_container');
            propLightbox.lightbox_container = document.getElementById('lightbox_container');
            propLightbox.lightbox_container.style.width = '100%';
            propLightbox.lightbox_container.style.height = '100%';
            propLightbox.lightbox_container.style.position = 'fixed';
            propLightbox.lightbox_container.style.zIndex = '1000';
            propLightbox.lightbox_container.style.background = 'rgba(0,0,0,0.8)';
            propLightbox.lightbox_container.style.top = '0';
            propLightbox.lightbox_container.style.left = '0';

            propLightbox.lightbox_container.appendChild(document.createElement('div')).setAttribute('id', 'modal');
            propLightbox.modal = document.getElementById('modal');
            propLightbox.modal.style.height = '100%';

            propLightbox.modal.appendChild(document.createElement('img')).setAttribute('src', propLightbox.imagenSrc);
            propLightbox.modal.getElementsByTagName('img')[0].setAttribute('class', 'imagen-modal');

            if (propLightbox.animacion == 'fade') {
                document.getElementsByClassName('imagen-modal')[0].style.opacity = 0;
                setTimeout(function () {
                    document.getElementsByClassName('imagen-modal')[0].style.opacity = 1;
                }, 100);
            }

            propLightbox.modal.innerHTML += '<i class="fa fa-times" id="close"></i>';
            propLightbox.cerrarModal = document.getElementById('close');
            propLightbox.cerrarModal.addEventListener('click', methodLightbox.cerrarModal)
        },
        cerrarModal: function () {
            propLightbox.cuerpoDom.removeChild(propLightbox.lightbox_container);
        }
    }

    methodLightbox.inicio();
})();