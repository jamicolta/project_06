// Propiedades Scroll
var propScroll = {
    posicion: window.pageYOffset,
    scrollMenu: document.getElementsByClassName('scrollMenu'),
    scrollTopMenu: document.getElementsByClassName('scrollTopMenu'),
    destino: null,
    seccion: null,
    intervalo: null
}
// Metodos Scroll
var methodScroll = {
    inicio: function () {
        for (var i = 0; i < propScroll.scrollMenu.length; i++) {
            propScroll.scrollMenu[i].addEventListener('click', function (e) {
                clearInterval(propScroll.intervalo);
                propScroll.destino = this.getAttribute('href');
                propScroll.seccion = document.querySelector(propScroll.destino).offsetTop - 94;

                propScroll.posicion = window.pageYOffset;
                propScroll.intervalo = setInterval(function () {
                    if (propScroll.posicion < propScroll.seccion) {
                        propScroll.posicion += 30;

                        if (propScroll.posicion >= propScroll.seccion) {
                            clearInterval(propScroll.intervalo);
                        }
                    } else {
                        propScroll.posicion -= 30;

                        if (propScroll.posicion <= propScroll.seccion) {
                            clearInterval(propScroll.intervalo);
                        }
                    }
                    window.scrollTo(0, propScroll.posicion);
                }, 15);

                e.preventDefault();
            });
        }

        for (var i = 0; i < propScroll.scrollTopMenu.length; i++) {
            clearInterval(propScroll.intervalo);
            propScroll.scrollTopMenu[i].addEventListener('click', function (e) {
                propScroll.posicion = window.pageYOffset;
                propScroll.intervalo = setInterval(function () {
                    if (propScroll.posicion > 0) {
                        propScroll.posicion -= 30;

                        if (propScroll.posicion <= 0) {
                            clearInterval(propScroll.intervalo);
                        }
                    } else {
                        return;
                    }
                    window.scrollTo(0, propScroll.posicion)
                }, 15);

                e.preventDefault();
            });
        }
    }
}

methodScroll.inicio();