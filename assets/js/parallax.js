(function () {
    // Propiedades Parallax
    var propParallax = {
        seccion: document.querySelector('.parallax'),
        recorrido: null,
        limite: null
    }
    // Metodos Parallax
    var methodParallax = {
        inicio: function () {
            window.addEventListener('scroll', function () {
                propParallax.recorrido = window.pageYOffset;
                propParallax.limite = propParallax.seccion.offsetTop + propParallax.seccion.offsetHeight;

                if (propParallax.recorrido > propParallax.seccion.offsetTop - window.outerHeight && propParallax.recorrido <= propParallax.limite) {
                    propParallax.seccion.style.backgroundPositionY = (propParallax.recorrido - propParallax.seccion.offsetTop) / 1.5 + 'px';
                } else {
                    propParallax.seccion.style.backgroundPositionY = 0;
                }
            });
        }
    }

    methodParallax.inicio();
})();