(function () {
    // Propiedades Formulario
    var propFormulario = {
        formulario: document.form_contacto,
        elementos: document.form_contacto.elements,
        error: null,
        textoError: null
    }
    // Metodos Formulario
    var methodFormulario = {
        inicio: function () {
            for (var i = 0; i < propFormulario.elementos.length; i++) {
                if (propFormulario.elementos[i].type == 'text' || propFormulario.elementos[i].type == 'email' || propFormulario.elementos[i].nodeName.toLowerCase() == 'textarea') {
                    propFormulario.elementos[i].addEventListener('focus', function () {
                        this.parentElement.children[1].className = 'label active';
                    });
                    propFormulario.elementos[i].addEventListener('blur', function () {
                        if (this.value == '') {
                            this.parentElement.children[1].className = 'label';
                        }
                    });
                }
            }
            propFormulario.formulario.addEventListener('submit', function (e) {
                for (var i = 0; i < propFormulario.elementos.length; i++) {
                    if (propFormulario.elementos[i].value == '') {
                        e.preventDefault();
                        if (propFormulario.elementos[i].parentElement.children.length < 3) {
                            propFormulario.error = document.createElement('p');
                            propFormulario.textoError = document.createTextNode('Completa los campos requeridos');
                            propFormulario.error.appendChild(propFormulario.textoError);
                            propFormulario.error.className = 'error';

                            propFormulario.elementos[i].parentElement.appendChild(propFormulario.error)
                        }
                    } else {
                        if (propFormulario.elementos[i].parentElement.children.length >= 3) {
                            propFormulario.error = propFormulario.elementos[i].parentElement.getElementsByTagName('p')[0];
                            propFormulario.elementos[i].parentElement.removeChild(propFormulario.error);
                        }
                    }
                }
            });
        }
    }

    methodFormulario.inicio();
})();